class Contact < MailForm::Base
  attribute :name, :validate => true
  attribute :email, :validate => /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i
  attribute :message
  attribute :nickname, :captcha => true

  #Declarate the e-mail headers. It accepts anything the mail method in ActionMailer accepts.
  #
  
  def headers
    {
    :subject => "My Contact Form",
    :to => "232516148@qip.ru",
    :from => %("#{name}" <#{email}>)
    }
  end
end

